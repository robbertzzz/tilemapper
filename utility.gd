class_name Utility


static func get_tile_rotation_from_bools(x_flipped: bool, y_flipped: bool, transposed: bool):
	if not (x_flipped or y_flipped or transposed):
		return 0
	
	if x_flipped and not y_flipped and transposed:
		return 90
	
	if x_flipped and y_flipped and not transposed:
		return 180
	
	if not x_flipped and y_flipped and transposed:
		return 270
	
	# Should not be reached
	return 0

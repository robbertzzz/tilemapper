class_name ChangeInstancePropertyCommand
extends Command


var tile_position: Vector2
var property_name: String
var old_value
var new_value


func execute(tile_map: TileMap = null, tile_mapper = null):
	tile_mapper.set_instance_property_value(tile_position, property_name, new_value)


func undo(tile_map: TileMap = null, tile_mapper = null):
	tile_mapper.set_instance_property_value(tile_position, property_name, old_value)

class_name InstanceProperty


var meta_property: MetaProperty


var property_type setget , get_property_type

var bool_value: bool
var int_value: int
var float_value: float
var string_value: String


func get_property_type() -> int:
	return meta_property.property_type


func _init(_meta_property: MetaProperty = null):
	if _meta_property:
		meta_property = _meta_property
		
		bool_value = meta_property.default_bool_value
		int_value = meta_property.default_int_value
		float_value = meta_property.default_float_value
		string_value = meta_property.default_string_value


func set_value(new_value):
	match self.property_type:
		MetaProperty.PropertyType.BOOL:
			bool_value = new_value
		MetaProperty.PropertyType.INT:
			int_value = new_value
		MetaProperty.PropertyType.FLOAT:
			float_value = new_value
		MetaProperty.PropertyType.STRING:
			string_value = new_value


func get_value():
	match self.property_type:
		MetaProperty.PropertyType.BOOL:
			return bool_value
		MetaProperty.PropertyType.INT:
			return int_value
		MetaProperty.PropertyType.FLOAT:
			return float_value
		MetaProperty.PropertyType.STRING:
			return string_value


func is_edited():
	return meta_property.get_default_value() != get_value()


func serialize() -> Dictionary:
	var serialized := {}
	
	if meta_property.get_default_value() != get_value():
		serialized["name"] = meta_property.property_name
		serialized["value"] = get_value()
	
	return serialized

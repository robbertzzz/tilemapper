extends Control


signal selected(id)

export var selected_color: Color
var id: int = -1
var size: Vector2


func set_tile(tile_set: TileSet, index: int):
	$Sprite.texture = tile_set.tile_get_texture(index)
	$Sprite.centered = false
	$Sprite.region_enabled = true
	$Sprite.region_rect = tile_set.tile_get_region(index)
	rect_min_size = $Sprite.region_rect.size
	size = rect_min_size
	id = index


func _gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		emit_signal("selected", id)


func select():
	$Sprite.modulate = selected_color


func deselect():
	$Sprite.modulate = Color.white


func rescale(new_scale: float):
	rect_min_size = new_scale * size
	rect_size = new_scale * size
	$Sprite.scale = Vector2.ONE * new_scale

# A container class that holds tile data for undo/redo
class_name TilePosition


var x: int
var y: int
var previous_index: int
var previous_flip_x: bool
var previous_flip_y: bool
var previous_transpose: bool
var previous_custom_properties := {}


func _init(_x: int, _y: int, _previous_index: int, _previous_flip_x: bool = false, _previous_flip_y: bool = false, _previous_transpose: bool = false, _previous_custom_properties = {}):
	x = _x
	y = _y
	previous_index = _previous_index
	previous_flip_x = _previous_flip_x
	previous_flip_y = _previous_flip_y
	previous_transpose = _previous_transpose
	previous_custom_properties = _previous_custom_properties

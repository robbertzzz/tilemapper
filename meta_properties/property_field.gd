extends HBoxContainer


signal property_delete(meta_property)
signal default_value_change(meta_property, new_value)


var meta_property: MetaProperty


func init(_meta_property: MetaProperty):
	meta_property = _meta_property
	$Name.text = meta_property.property_name
	$Default.set("value", meta_property.get_default_value())
	$Default.set("text", meta_property.get_default_value())
	$Default.set("pressed", meta_property.get_default_value())


func _on_Default_toggled(button_pressed):
	emit_signal("default_value_change", meta_property, button_pressed)


func _on_Default_value_changed(value):
	emit_signal("default_value_change", meta_property, value)


# For the String version, listening to text change would be horrible for undo/redo
func _on_Default_focus_exited():
	if meta_property.default_string_value != $Default.text:
		emit_signal("default_value_change", meta_property, $Default.text)


func _on_DeleteBtn_pressed():
	emit_signal("property_delete", meta_property)

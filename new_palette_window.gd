extends PanelContainer


signal tile_set_created(texture, width, height)
signal tile_set_creation_cancelled()

var selected_texture: ImageTexture


func _ready():
	hide()


func _on_LoadTextureBtn_pressed():
	$CenterContainer/FileDialog.show()


func _on_FileDialog_file_selected(path):
	var image := Image.new()
	image.load(path)
	selected_texture = ImageTexture.new()
	selected_texture.create_from_image(image)


func _on_AcceptBtn_pressed():
	hide()
	var width = $CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/WidthSpinBox.value
	var height = $CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/HeightSpinBox.value
	if selected_texture == null or width == 0 or height == 0:
		push_error("Wrong selection")
		emit_signal("tile_set_creation_cancelled")
		return
	
	emit_signal("tile_set_created", selected_texture, width, height)


func _on_CancelBtn_pressed():
	hide()
	
	emit_signal("tile_set_creation_cancelled")

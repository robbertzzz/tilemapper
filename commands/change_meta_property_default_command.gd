class_name ChangeMetaPropertyDefaultCommand
extends Command


var tile_type
var property_name
var old_default_value
var new_default_value


func execute(tile_map: TileMap = null, tile_mapper = null):
	tile_mapper.set_meta_property_default_value(tile_type, property_name, new_default_value)


func undo(tile_map: TileMap = null, tile_mapper = null):
	tile_mapper.set_meta_property_default_value(tile_type, property_name, old_default_value)

class_name MetaProperty


enum PropertyType {
	BOOL,
	INT,
	FLOAT,
	STRING,
}

var property_name: String
var property_type = PropertyType.BOOL

var default_bool_value: bool
var default_int_value: int
var default_float_value: float
var default_string_value: String


func get_default_value():
	match property_type:
		PropertyType.BOOL:
			return default_bool_value
		PropertyType.INT:
			return default_int_value
		PropertyType.FLOAT:
			return default_float_value
		PropertyType.STRING:
			return default_string_value


func set_default_value(value):
	match property_type:
		PropertyType.BOOL:
			default_bool_value = value
		PropertyType.INT:
			default_int_value = value
		PropertyType.FLOAT:
			default_float_value = value
		PropertyType.STRING:
			default_string_value = value


func serialize() -> Dictionary:
	var serialized := {}
	serialized["name"] = property_name
	match property_type:
		PropertyType.BOOL:
			serialized["type"] = "bool"
			serialized["default_value"] = default_bool_value
		PropertyType.INT:
			serialized["type"] = "int"
			serialized["default_value"] = default_int_value
		PropertyType.FLOAT:
			serialized["type"] = "float"
			serialized["default_value"] = default_float_value
		PropertyType.STRING:
			serialized["type"] = "string"
			serialized["default_value"] = default_string_value
	return serialized


func deserialize(serialized: Dictionary):
	property_name = serialized["name"]
	match serialized["type"]:
		"bool":
			property_type = PropertyType.BOOL
			default_bool_value = serialized["default_value"]
		"int":
			property_type = PropertyType.INT
			default_int_value = serialized["default_value"]
		"float":
			property_type = PropertyType.FLOAT
			default_float_value = serialized["default_value"]
		"string":
			property_type = PropertyType.STRING
			default_string_value = serialized["default_value"]

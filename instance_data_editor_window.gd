extends PanelContainer


signal instance_properties_changed()

onready var tile_mapper = $"../.."
onready var properties_container = $CenterContainer/PanelContainer/VBoxContainer/PanelContainer/VBoxContainer/ScrollContainer/Properties


func _ready():
	hide()


func show_for_tile(tile_position: Vector2, tile_type: int):
	show()
	
	tile_mapper.verify_tile_properties(tile_type, tile_position.x, tile_position.y)
	
	var meta_properties = tile_mapper.get_meta_properties(tile_type)
	var instance_properties = tile_mapper.get_instance_properties(tile_position)
	
	while properties_container.get_child_count() > 0:
		var item = properties_container.get_child(0)
		properties_container.remove_child(item)
		item.queue_free()
	
	for meta_property in meta_properties:
		var instance_property = instance_properties[meta_property]
		var property_field: InstancePropertyField
		match meta_property.property_type:
			MetaProperty.PropertyType.BOOL:
				property_field = preload("res://instance_properties/bool_instance_property.tscn").instance()
			MetaProperty.PropertyType.INT:
				property_field = preload("res://instance_properties/int_instance_property.tscn").instance()
			MetaProperty.PropertyType.FLOAT:
				property_field = preload("res://instance_properties/float_instance_property.tscn").instance()
			MetaProperty.PropertyType.STRING:
				property_field = preload("res://instance_properties/string_instance_property.tscn").instance()
		
		properties_container.add_child(property_field)
		property_field.init(instance_property)
		property_field.connect("value_change", self, "on_property_value_changed", [tile_position])


func on_property_value_changed(instance_property: InstanceProperty, new_value, tile_position: Vector2):
	print("changed")
#	set_instance_property_value(tile_position: Vector2, property_name: String, new_value, add_to_undo_list: bool = false):
	tile_mapper.set_instance_property_value(tile_position, instance_property.meta_property.property_name, new_value, true)


func _on_CloseBtn_pressed():
	emit_signal("instance_properties_changed")
	hide()

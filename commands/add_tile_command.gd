class_name AddTileCommand
extends Command


var tile_positions: Array

var tile_index: int
var flip_x: bool
var flip_y: bool
var transpose: bool


func _init(_tile_index: int, _flip_x: bool, _flip_y: bool, _transpose: bool):
	tile_index = _tile_index
	flip_x = _flip_x
	flip_y = _flip_y
	transpose = _transpose


func add_tile_position(map, tile_map: TileMap, x: int, y: int) -> bool:
	if tile_map.get_cell(x, y) != tile_index \
			or flip_x != tile_map.is_cell_x_flipped(x, y) \
			or flip_y != tile_map.is_cell_y_flipped(x, y) \
			or transpose != tile_map.is_cell_transposed(x, y):
		tile_positions.append(.get_tile_position(map, tile_map, x, y))
		return true
	return false


func execute(tile_map: TileMap = null, tile_mapper = null):
	for tile_position in tile_positions:
		tile_map.set_cell(
			tile_position.x,
			tile_position.y,
			tile_index,
			flip_x,
			flip_y,
			transpose
			)
		tile_mapper.erase_instance_properties(Vector2(tile_position.x, tile_position.y))


func undo(tile_map: TileMap = null, tile_mapper = null):
	for tile_position in tile_positions:
		tile_map.set_cell(
			tile_position.x,
			tile_position.y,
			tile_position.previous_index,
			tile_position.previous_flip_x,
			tile_position.previous_flip_y,
			tile_position.previous_transpose
		)
		for key in tile_position.previous_custom_properties:
			var meta_property: MetaProperty = tile_mapper.get_meta_property_by_name(tile_position.previous_index, key)
			var instance_property = InstanceProperty.new(meta_property)
			instance_property.set_value(tile_position.previous_custom_properties[key])
			tile_mapper.set_instance_property(Vector2(tile_position.x, tile_position.y), instance_property)

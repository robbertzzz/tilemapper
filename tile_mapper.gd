extends Node


enum MenuOptions {
	NEW,
	LOAD,
	SAVE,
	SAVE_AS,
	QUIT,
}

onready var map = $Map
onready var palette = $UI/UI/HBoxContainer/PaletteMenu/VBoxContainer/PanelContainer/ScrollContainer/Palette
onready var meta_data_editor_window = $UI/MetaDataEditorWindow
onready var quit_confirmation_parent = $UI/QuitConfirmationParent
onready var quit_confirmation_dialog = $UI/QuitConfirmationParent/CenterContainer/QuitConfirmationDialog

var meta_properties: Dictionary = {} # [tile_type, Array[MetaProperty]]
var instance_properties: Dictionary = {} # [TilePosition, Dictionary[MetaProperty, InstanceProperty]]

var save_location: String = ""
var load_location: String = "" # A buffer variable to store the location when prompting the user they're sure they want to quit the current map without saving

var palette_scale: float = 1

var has_unsaved_changes: bool = false


func _ready():
	get_tree().set_auto_accept_quit(false)
	create_tile_set(preload("res://temp_tileset.png"), 16, 16)
	map.connect("tool_changed", self, "on_map_tool_changed")
	map.connect("tile_properties_selected", self, "on_map_tile_properties_selected")
	map.connect("brush_changed", self, "on_map_brush_changed")
	create_file_menu()


func create_file_menu():
	var file_button = $UI/UI/TopMenu/HBoxContainer2/HBoxContainer/FileBtn
	file_button.get_popup().add_item("New")
	file_button.get_popup().add_item("Load...")
	file_button.get_popup().add_item("Save")
	file_button.get_popup().add_item("Save As...")
	file_button.get_popup().add_item("Quit")
	
	var popup_menu: PopupMenu = file_button.get_popup()
	popup_menu.connect("id_pressed", self, "on_file_menu_id_pressed")


func on_file_menu_id_pressed(id: int):
	match id:
		MenuOptions.NEW:
			print("new")
			on_before_new_map()
		MenuOptions.LOAD:
			print("load")
			prompt_load_map()
		MenuOptions.SAVE:
			print("save")
			save_map()
		MenuOptions.SAVE_AS:
			print("save as")
			save_map_as()
		MenuOptions.QUIT:
			on_before_quit()


func add_meta_property(tile_type: int, property: MetaProperty, add_to_undo_list: bool = false):
	if not tile_type in meta_properties:
		meta_properties[tile_type] = []
	meta_properties[tile_type].append(property)
	meta_data_editor_window.refresh()
	
	if add_to_undo_list:
		var command = AddMetaPropertyCommand.new()
		command.tile_type = tile_type
		command.property_name = property.property_name
		command.property_type = property.property_type
		command.default_value = property.get_default_value()
		map.add_undo_command(command)


func remove_meta_property(tile_type: int, property: MetaProperty, add_to_undo_list: bool = false):
	if not tile_type in meta_properties:
		return
	meta_properties[tile_type].erase(property)
	meta_data_editor_window.refresh()
	
	if add_to_undo_list:
		var command := RemoveMetaPropertyCommand.new()
		command.property_name = property.property_name
		command.property_type = property.property_type
		command.default_value = property.get_default_value()
		map.add_undo_command(command)


func get_meta_properties(tile_type: int) -> Array:
	if not tile_type in meta_properties:
		return []
	return meta_properties[tile_type]


func get_meta_property_by_name(tile_type: int, property_name: String) -> MetaProperty:
	if not tile_type in meta_properties:
		return null
	for meta_property in meta_properties[tile_type]:
		if meta_property.property_name == property_name:
			return meta_property
	return null


func set_meta_property_default_value(tile_type: int, property_name: String, new_value, add_to_undo_list: bool = false):
	var meta_property := get_meta_property_by_name(tile_type, property_name)
	if not meta_property:
		return
	
	if add_to_undo_list:
		var command := ChangeMetaPropertyDefaultCommand.new()
		command.tile_type = tile_type
		command.property_name = property_name
		command.old_default_value = meta_property.get_default_value()
		command.new_default_value = new_value
		map.add_undo_command(command)
	
	meta_property.set_default_value(new_value)
	map.refresh_metadata_overlay()


func get_instance_properties(tile_position: Vector2) -> Dictionary:
	if not tile_position in instance_properties:
		return {}
	return instance_properties[tile_position]


func set_instance_property(tile_position: Vector2, instance_property: InstanceProperty):
	if not tile_position in instance_properties:
		instance_properties[tile_position] = {}
	instance_properties[tile_position][instance_property.meta_property] = instance_property
	map.refresh_metadata_overlay()


func set_instance_property_value(tile_position: Vector2, property_name: String, new_value, add_to_undo_list: bool = false):
	assert(tile_position in instance_properties)
	
	var meta_property = get_meta_property_by_name(map.tile_map.get_cellv(tile_position), property_name)
	var instance_property: InstanceProperty = instance_properties[tile_position][meta_property]
	
	if add_to_undo_list:
		var command := ChangeInstancePropertyCommand.new()
		command.tile_position = tile_position
		command.property_name = property_name
		command.old_value = instance_property.get_value()
		command.new_value = new_value
		map.add_undo_command(command)
	
	instance_property.set_value(new_value)
	map.refresh_metadata_overlay()


func erase_instance_properties(tile_position: Vector2):
	instance_properties.erase(tile_position)
	map.refresh_metadata_overlay()


func on_map_tool_changed(new_tool):
	match new_tool:
		Map.Tool.PLACE:
			$UI/UI/TopMenu/HBoxContainer2/Tools/PlaceTiles.pressed = true
		Map.Tool.RECTANGLE_PLACE:
			$UI/UI/TopMenu/HBoxContainer2/Tools/RectanglePlaceTiles.pressed = true
		Map.Tool.BRUSH:
			$UI/UI/TopMenu/HBoxContainer2/Tools/Brush.pressed = true
		Map.Tool.ERASE:
			$UI/UI/TopMenu/HBoxContainer2/Tools/Eraser.pressed = true
		Map.Tool.SELECT:
			$UI/UI/TopMenu/HBoxContainer2/Tools/Select.pressed = true
		Map.Tool.METADATA:
			$UI/UI/TopMenu/HBoxContainer2/Tools/Metadata.pressed = true


func on_map_tile_properties_selected(tile_position: Vector2, tile_type: int):
	$UI/InstanceDataEditorWindow.show_for_tile(tile_position, tile_type)


func on_map_brush_changed(brush: Array):
	$UI/UI/TopMenu/HBoxContainer2/Tools/Brush.disabled = brush == []


func load_tile_set():
	var tile_set = TileSet.new()
	# Load the tileset's data


func save_tile_set():
	var tile_set: TileSet = map.get_tile_set()


func create_tile_set(texture: Texture, tile_width: int, tile_height: int):
	var tile_set = TileSet.new()
	var texture_width: int = texture.get_width()
	var texture_height: int = texture.get_height()
	var image: Image = texture.get_data()
	image.lock()
	
	var id: int = 0
	for y in texture_height / tile_height:
		for x in texture_width / tile_width:
			# loop through pixels to make sure this isn't a transparent square
			var is_transparent: bool = true
			for py in tile_height:
				for px in tile_width:
					if image.get_pixel(x * tile_width + px, y * tile_height + py).a != 0:
						is_transparent = false
			
			if not is_transparent:
				tile_set.create_tile(id)
				tile_set.tile_set_texture(id, texture)
				tile_set.tile_set_region(id, Rect2(x * tile_width, y * tile_height, tile_width, tile_height))
				id += 1
	
	map.set_tile_set(tile_set, tile_width, tile_height)
	create_palette()
	meta_data_editor_window.create_palette(map.get_tile_set())


func prompt_load_map():
	$UI/FileDialogs.show()
	$UI/FileDialogs/CenterContainer/LoadFileDialog.show()


func new_map():
	get_tree().change_scene("res://tile_mapper.tscn")


func load_map():
	var file := File.new()
	file.open(load_location, File.READ)
	var content = file.get_as_text()
	file.close()
	
	var data: Dictionary = parse_json(content)
	print(data)
	
	meta_properties = {}
	instance_properties = {}
	
	for key in data.meta_properties.keys():
		var tile_id: int = key.to_int()
		for meta_property_data in data.meta_properties[key]:
			var meta_property := MetaProperty.new()
			meta_property.deserialize(meta_property_data)
			add_meta_property(tile_id, meta_property)
	
	map.load_tile_data(data.tiles)
	
	for tile in data.tiles:
		if "instance_properties" in tile:
			var tile_position = Vector2(tile.position.x, tile.position.y)
			instance_properties[tile_position] = {}
			
			for key in tile.instance_properties.keys():
				for meta_property in meta_properties[int(tile.type)]:
					if meta_property.property_name == key:
						var instance_property := InstanceProperty.new(meta_property)
						instance_property.set_value(tile.instance_properties[key])
						instance_properties[tile_position][meta_property] = instance_property
	
	has_unsaved_changes = false


func save_map():
	if save_location == "":
		save_map_as()
		return
	
	var serialized_meta_properties: Dictionary
	for tile_id in meta_properties.keys():
		var tile_properties = []
		for meta_property in meta_properties[tile_id]:
			tile_properties.append(meta_property.serialize())
		serialized_meta_properties[tile_id] = tile_properties
	
	var all_tiles: Array = map.get_all_tiles()
	var save_tiles: Array
	for tile_data in all_tiles:
		var tile := {}
		tile["position"] = {
			x = tile_data.x,
			y = tile_data.y
		}
		tile["type"] = tile_data.tile_type
		tile["rotation"] = tile_data.rotation
		tile["instance_properties"] = {}
		
		var tile_position = Vector2(tile_data.x, tile_data.y)
		if tile_position in instance_properties and tile_data.tile_type in meta_properties:
			for key in instance_properties[tile_position].keys():
				if key in meta_properties[tile_data.tile_type]:
					var serialized = instance_properties[tile_position][key].serialize()
					if serialized.size() > 0:
						tile["instance_properties"][serialized.name] = serialized.value
		
		if tile["instance_properties"].size() == 0:
			tile.erase("instance_properties")
		
		save_tiles.append(tile)
	
	var save_data = {
		meta_properties = serialized_meta_properties,
		tiles = save_tiles
	}
	
	var save_string = JSON.print(save_data, "\t")
	
	print(save_string)
	
	var file := File.new()
	if file.open(save_location, File.WRITE) == OK:
		file.store_string(save_string)
		file.close()
	
	has_unsaved_changes = false


func save_map_as():
	$UI/FileDialogs.show()
	$UI/FileDialogs/CenterContainer/SaveFileDialog.show()


func create_palette():
	while palette.get_child_count() > 0:
		var item = palette.get_child(0)
		palette.remove_child(item)
		item.queue_free()
	
	var tile_set: TileSet = map.get_tile_set()
	for tile_id in tile_set.get_tiles_ids():
		var item = preload("res://palette_item.tscn").instance()
		palette.add_child(item)
		item.set_tile(tile_set, tile_id)
		item.connect("selected", self, "on_palette_item_selected")
	on_palette_item_selected(0)


func verify_tile_properties(tile_type: int, x: int, y: int):
	var tile_position := Vector2(x, y)
	
	var tile_instance_properties: Dictionary = get_instance_properties(tile_position)
	var tile_meta_properties: Array = get_meta_properties(tile_type)
	
	for meta_property in tile_meta_properties:
		if not tile_instance_properties.has(meta_property):
			var instance_property := InstanceProperty.new(meta_property)
			tile_instance_properties[meta_property] = instance_property
	
	for key in tile_instance_properties.keys():
		if not key in tile_meta_properties:
			tile_instance_properties.erase(key)
	
	instance_properties[tile_position] = tile_instance_properties
	
	if instance_properties[tile_position].size() == 0:
		# No need to have an empty item in this array
		instance_properties.erase(tile_position)


func verify_all_tile_properties():
	var all_tiles = map.get_all_tiles()
	for tile in all_tiles:
		verify_tile_properties(tile.tile_type, tile.x, tile.y)


func on_palette_item_selected(item_id: int):
	for item in palette.get_children():
		if item.id == item_id:
			item.select()
		else:
			item.deselect()
	map.select_palette_item(item_id)


func _on_PlaceTiles_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.PLACE)


func _on_RectanglePlaceTiles_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.RECTANGLE_PLACE)


func _on_Brush_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.BRUSH)


func _on_Eraser_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.ERASE)


func _on_Select_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.SELECT)


func _on_Metadata_toggled(button_pressed):
	if button_pressed:
		map.set_tool(map.Tool.METADATA)


func _on_NewTileSet_pressed():
	$UI/NewPaletteWindow.connect("tile_set_created", self, "on_tile_set_created")
	$UI/NewPaletteWindow.connect("tile_set_creation_cancelled", self, "on_tile_set_creation_cancelled")
	$UI/NewPaletteWindow.show()


func on_tile_set_created(texture: Texture, tile_width: int, tile_height: int):
	$UI/NewPaletteWindow.disconnect("tile_set_created", self, "on_tile_set_created")
	$UI/NewPaletteWindow.disconnect("tile_set_creation_cancelled", self, "on_tile_set_creation_cancelled")
	create_tile_set(texture, tile_width, tile_height)


func on_tile_set_creation_cancelled():
	$UI/NewPaletteWindow.disconnect("tile_set_created", self, "on_tile_set_created")
	$UI/NewPaletteWindow.disconnect("tile_set_creation_cancelled", self, "on_tile_set_creation_cancelled")


func _on_UndoBtn_pressed():
	map.undo()


func _on_RedoBtn_pressed():
	map.redo()


func _on_EditMetaData_pressed():
	meta_data_editor_window.show()


func _on_SaveFileDialog_file_selected(path):
	save_location = path
	save_map()


func _on_LoadFileDialog_file_selected(path):
	load_location = path
	on_before_load()


func _on_FileDialog_hide():
	$UI/FileDialogs.hide()


func _on_ScalePaletteUp_pressed():
	if palette_scale >= 1:
		set_palette_scale(palette_scale + 1)
	else:
		set_palette_scale(palette_scale * 2)


func _on_ScalepaletteDown_pressed():
	if palette_scale > 1:
		set_palette_scale(palette_scale - 1)
	else:
		set_palette_scale(palette_scale * 0.5)


func _on_ScalePalette1x_pressed():
	set_palette_scale(1)


func set_palette_scale(new_scale: float):
	palette_scale = new_scale
	for palette_item in palette.get_children():
		palette_item.rescale(palette_scale)


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		on_before_quit()


func on_before_new_map():
	if has_unsaved_changes:
		quit_confirmation_parent.show()
		quit_confirmation_dialog.show()
		quit_confirmation_dialog.connect("hide", self, "on_new_map_cancelled")
		quit_confirmation_dialog.connect("confirmed", self, "new_map")
	else:
		new_map()


func on_new_map_cancelled():
	quit_confirmation_dialog.call_deferred("disconnect", "confirmed", self, "new_map")
	quit_confirmation_dialog.disconnect("hide", self, "on_new_map_cancelled")
	quit_confirmation_parent.hide()


func on_before_load():
	if has_unsaved_changes:
		quit_confirmation_parent.show()
		quit_confirmation_dialog.show()
		quit_confirmation_dialog.connect("hide", self, "on_load_cancelled")
		quit_confirmation_dialog.connect("confirmed", self, "load_map")
	else:
		load_map()


func on_load_cancelled():
	quit_confirmation_dialog.call_deferred("disconnect", "confirmed", self, "load_map")
	quit_confirmation_dialog.disconnect("hide", self, "on_load_cancelled")
	quit_confirmation_parent.hide()


func on_before_quit():
	if has_unsaved_changes:
		print("you sure?")
		quit_confirmation_parent.show()
		quit_confirmation_dialog.show()
		quit_confirmation_dialog.connect("hide", self, "on_quit_cancelled")
		quit_confirmation_dialog.connect("confirmed", self, "quit")
	else:
		quit()


func quit():
	get_tree().quit()


func on_quit_cancelled():
	quit_confirmation_dialog.call_deferred("disconnect", "confirmed", self, "quit")
	quit_confirmation_dialog.disconnect("hide", self, "on_quit_cancelled")
	quit_confirmation_parent.hide()

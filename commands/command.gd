class_name Command


func execute(tile_map: TileMap = null, tile_mapper = null):
	pass


func undo(tile_map: TileMap = null, tile_mapper = null):
	pass


func get_tile_position(map, tile_map: TileMap, x: int, y: int):
	var tile_mapper = map.tile_mapper
	var instance_properties := {}
	var tile_position = Vector2(x, y)
	if tile_mapper.instance_properties.has(tile_position):
		for meta_property in tile_mapper.instance_properties[tile_position]:
			instance_properties[meta_property.property_name] = tile_mapper.instance_properties[tile_position][meta_property].get_value()
	return TilePosition.new(x, y, tile_map.get_cell(x, y), tile_map.is_cell_x_flipped(x, y), tile_map.is_cell_y_flipped(x, y), tile_map.is_cell_transposed(x, y), instance_properties)

class_name AddBrushCommand
extends Command


var brush: Array
var brush_x: int
var brush_y: int

var tile_positions: Array = []


func execute(tile_map: TileMap = null, tile_mapper = null):
	tile_positions = []
	
	for y in brush.size():
		for x in brush[y].size():
			tile_positions.append(.get_tile_position(tile_mapper.map, tile_map, x + brush_x, y + brush_y))
			
			var tile_data = brush[y][x]
			if tile_data:
				tile_map.set_cell(
					x + brush_x,
					y + brush_y,
					tile_data.tile_type,
					tile_data.flip_x,
					tile_data.flip_y,
					tile_data.transposed
				)
			else:
				tile_map.set_cell(
					x + brush_x,
					y + brush_y,
					-1
				)


func undo(tile_map: TileMap = null, tile_mapper = null):
	for tile_position in tile_positions:
		tile_map.set_cell(
			tile_position.x,
			tile_position.y,
			tile_position.previous_index,
			tile_position.previous_flip_x,
			tile_position.previous_flip_y,
			tile_position.previous_transpose
		)
		for key in tile_position.previous_custom_properties:
			var meta_property: MetaProperty = tile_mapper.get_meta_property_by_name(tile_position.previous_index, key)
			var instance_property = InstanceProperty.new(meta_property)
			instance_property.set_value(tile_position.previous_custom_properties[key])
			tile_mapper.set_instance_property(Vector2(tile_position.x, tile_position.y), instance_property)

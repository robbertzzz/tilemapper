extends PanelContainer


signal closed()

onready var tile_mapper = $"../.."
onready var palette = $CenterContainer/PanelContainer/VBoxContainer/Palette
onready var properties_container = $CenterContainer/PanelContainer/VBoxContainer/PanelContainer/VBoxContainer/ScrollContainer/Properties

var selected_tile_type: int = 0


func _ready():
	hide()


func create_palette(tile_set: TileSet):
	while palette.get_child_count() > 0:
		var item = palette.get_child(0)
		palette.remove_child(item)
		item.queue_free()
	
	for tile_id in tile_set.get_tiles_ids():
		var item = preload("res://palette_item.tscn").instance()
		palette.add_child(item)
		item.set_tile(tile_set, tile_id)
		item.connect("selected", self, "on_palette_item_selected")
	on_palette_item_selected(0)


func on_palette_item_selected(item_id: int):
	for item in palette.get_children():
		if item.id == item_id:
			item.select()
		else:
			item.deselect()
	
	selected_tile_type = item_id
	
	while properties_container.get_child_count() > 0:
		var item = properties_container.get_child(0)
		properties_container.remove_child(item)
		item.queue_free()
	
	if item_id in tile_mapper.meta_properties:
		var properties = tile_mapper.meta_properties[item_id]
		for property in properties:
			var property_field
			match property.property_type:
				MetaProperty.PropertyType.BOOL:
					property_field = preload("res://meta_properties/bool_property.tscn").instance()
				MetaProperty.PropertyType.INT:
					property_field = preload("res://meta_properties/int_property.tscn").instance()
				MetaProperty.PropertyType.FLOAT:
					property_field = preload("res://meta_properties/float_property.tscn").instance()
				MetaProperty.PropertyType.STRING:
					property_field = preload("res://meta_properties/string_property.tscn").instance()
			properties_container.add_child(property_field)
			property_field.init(property)
			property_field.connect("property_delete", self, "on_property_delete")
			property_field.connect("default_value_change", self, "on_default_value_changed")


func show():
	.show()
	refresh()
	$CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/PropertyName.grab_focus()


func refresh():
	if visible:
		on_palette_item_selected(selected_tile_type)


func _on_Add_pressed():
	var property_name = $CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/PropertyName.text
	
	if property_name == "":
		return
	
	if tile_mapper.meta_properties.has(selected_tile_type):
		for property in tile_mapper.meta_properties[selected_tile_type]:
			if property.property_name == property_name:
				return
	
	var property_type = $CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/OptionButton.selected
	var meta_property := MetaProperty.new()
	meta_property.property_type = property_type
	meta_property.property_name = property_name
	tile_mapper.add_meta_property(selected_tile_type, meta_property, true)
	
	$CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/PropertyName.text = ""
	$CenterContainer/PanelContainer/VBoxContainer/HBoxContainer/PropertyName.grab_focus()


func on_property_delete(meta_property: MetaProperty):
	tile_mapper.remove_meta_property(selected_tile_type, meta_property, true)


func on_default_value_changed(meta_property: MetaProperty, new_value):
	tile_mapper.set_meta_property_default_value(selected_tile_type, meta_property.property_name, new_value, true)


func _on_CloseBtn_pressed():
	emit_signal("closed")
	hide()

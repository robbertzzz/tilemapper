class_name Map
extends Node2D


signal tool_changed(new_tool)
signal tile_properties_selected(tile_position, tile_type)
signal brush_changed(new_brush)


enum Tool {
	PLACE,
	RECTANGLE_PLACE,
	BRUSH,
	ERASE,
	SELECT,
	METADATA,
}

onready var tile_mapper = $".."
onready var camera = $Camera2D
onready var tile_map = $TileMap
onready var overlay_tile_map = $OverlayTileMap
onready var preview = $Preview
onready var hover_cursor = $HoverCursor
onready var selection_box = $SelectionBox
onready var meta_data_overlay = $MetaDataOverlay

var current_tool: int

var is_placing: bool = false
var is_box_placing: bool = false
var is_moving: bool = false
var is_selecting: bool = false
var has_selection: bool = false
var selection_start_tile: Vector2 # In tile space
var selection_end_tile: Vector2 # In tile space
var tile_rotation_index: int = 0

var brush: Array = [] # two-dimensional array of TileData

var palette_tile_index: int = 0
var _tile_index: int = 0
var _tile_width: int = 32
var _tile_height: int = 32

var camera_move_start: Vector2
var camera_move_mouse_start: Vector2
var camera_zoom: float = 1.0
var max_camera_zoom: float = 4.0
var min_camera_zoom: float = 0.1

var current_command: Command
var undo_history: Array
var redo_history: Array


func _unhandled_input(event):
	if event is InputEventMouse:
		var world_position = camera_zoom * (event.position - 0.5 * camera.get_viewport_rect().size) + camera.position
		var screen_position = event.position
		var tile_position = Vector2(floor(world_position.x / _tile_width), floor(world_position.y / _tile_height))
		preview.position = Vector2(_tile_width, _tile_height) * (0.5 * Vector2.ONE + tile_position)
		hover_cursor.rect_position = Vector2(_tile_width, _tile_height) * tile_position
		
		if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
			is_placing = false
			is_moving = event.pressed
			camera_move_start = camera.position
			camera_move_mouse_start = screen_position
		
		if is_moving:
			camera.position = camera_move_start - camera_zoom * (screen_position - camera_move_mouse_start)
		
		if current_tool == Tool.PLACE or current_tool == Tool.ERASE:
			handle_mouse_input_place_erase(event, world_position, screen_position, tile_position)
		elif current_tool == Tool.RECTANGLE_PLACE:
			handle_mouse_input_rectangle_place(event, world_position, screen_position, tile_position)
		elif current_tool == Tool.BRUSH:
			handle_mouse_input_brush(event, world_position, screen_position, tile_position)
		elif current_tool == Tool.SELECT:
			handle_mouse_input_select(event, world_position, screen_position, tile_position)
		elif current_tool == Tool.METADATA:
			handle_mouse_input_metadata(event, world_position, screen_position, tile_position)
		
		if event is InputEventMouseButton:
			if event.button_index == BUTTON_WHEEL_DOWN and camera_zoom < max_camera_zoom:
				zoom_out()
			elif event.button_index == BUTTON_WHEEL_UP and camera_zoom > min_camera_zoom:
				zoom_in()
	
	if event is InputEventKey and event.pressed:
		if event.physical_scancode == KEY_Z and not event.control:
			# Rotate CCW
			preview.rotation_degrees -= 90
			tile_rotation_index = (tile_rotation_index + 3) % 4
		elif event.physical_scancode == KEY_X and not event.control:
			# Rotate CW
			preview.rotation_degrees += 90
			tile_rotation_index = (tile_rotation_index + 1) % 4
		
		if event.scancode == KEY_T:
			set_tool(Tool.PLACE, true)
		elif event.scancode == KEY_R:
			set_tool(Tool.RECTANGLE_PLACE, true)
		elif event.scancode == KEY_B or (event.scancode == KEY_V and event.control):
			set_tool(Tool.BRUSH, true)
		elif event.scancode == KEY_E:
			set_tool(Tool.ERASE, true)
		elif event.scancode == KEY_S and not event.control:
			set_tool(Tool.SELECT, true)
		elif event.scancode == KEY_M:
			set_tool(Tool.METADATA, true)
		
		if event.scancode == KEY_Z and event.control and not event.shift:
			undo()
		elif event.scancode == KEY_Z and event.control and event.shift:
			redo()
		elif event.scancode == KEY_Y and event.control and not event.shift:
			redo()
		
		if event.scancode == KEY_C and event.control and has_selection:
			create_brush_from_selected()
		
		if event.scancode == KEY_X and event.control and has_selection:
			create_brush_from_selected()
			delete_selected_tiles()
		
		if event.scancode == KEY_DELETE:
			delete_selected_tiles()
		
		
		if event.scancode == KEY_S and event.control and not event.shift:
			tile_mapper.save_map()
		elif event.scancode == KEY_O and event.control and not event.shift:
			tile_mapper.prompt_load_map()
		elif event.scancode == KEY_S and event.control and event.shift:
			tile_mapper.save_map_as()


func handle_mouse_input_place_erase(event: InputEvent, world_position: Vector2, screen_position: Vector2, tile_position: Vector2):
	if not is_moving and event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		is_placing = event.pressed
		if is_placing:
			current_command = AddTileCommand.new(_tile_index, tile_rotation_index == 1 or tile_rotation_index == 2, tile_rotation_index > 1, tile_rotation_index % 2 == 1)
		else:
			if current_command and current_command.tile_positions.size() > 0:
				add_undo_command(current_command)
			current_command = null
	
	if not is_moving and is_placing:
		current_command.add_tile_position(self, tile_map, tile_position.x, tile_position.y)
		tile_mapper.erase_instance_properties(Vector2(tile_position.x, tile_position.y))
		var x_flipped := tile_rotation_index == 1 or tile_rotation_index == 2
		var y_flipped := tile_rotation_index > 1
		var transposed := tile_rotation_index % 2 == 1
		tile_map.set_cellv(tile_position, _tile_index, x_flipped, y_flipped, transposed)


func handle_mouse_input_rectangle_place(event: InputEvent, world_position: Vector2, screen_position: Vector2, tile_position: Vector2):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		is_box_placing = event.pressed
		if is_box_placing:
			selection_start_tile = tile_position
			selection_end_tile = tile_position
			preview.hide()
		else:
			# Place tiles
			selection_end_tile = tile_position
			var start_tile := Vector2(
				min(selection_start_tile.x, selection_end_tile.x),
				min(selection_start_tile.y, selection_end_tile.y)
			)
			var end_tile := Vector2(
				max(selection_start_tile.x, selection_end_tile.x),
				max(selection_start_tile.y, selection_end_tile.y)
			)
			
			var flip_x := tile_rotation_index == 1 or tile_rotation_index == 2
			var flip_y := tile_rotation_index > 1
			var transposed := tile_rotation_index % 2 == 1
			var place_command := AddTileCommand.new(_tile_index, flip_x, flip_y, transposed)
			
			for y in range(start_tile.y, end_tile.y + 1):
				for x in range(start_tile.x, end_tile.x + 1):
					place_command.add_tile_position(self, tile_map, x, y)
			
			place_command.execute(tile_map)
			add_undo_command(place_command)
			
			clear_tile_map_preview()
			preview.show()
	if is_box_placing:
		selection_end_tile = tile_position
		var start_tile := Vector2(
			min(selection_start_tile.x, selection_end_tile.x),
			min(selection_start_tile.y, selection_end_tile.y)
		)
		var end_tile := Vector2(
			max(selection_start_tile.x, selection_end_tile.x),
			max(selection_start_tile.y, selection_end_tile.y)
		)
		var tiles := []
		for y in range(start_tile.y, end_tile.y + 1):
			for x in range(start_tile.x, end_tile.x + 1):
				var tile_data := TileData.new(x, y, _tile_index)
				tile_data.flip_x = tile_rotation_index == 1 or tile_rotation_index == 2
				tile_data.flip_y = tile_rotation_index > 1
				tile_data.transposed = tile_rotation_index % 2 == 1
				tiles.append(tile_data)
		preview_tiles(tiles)


func handle_mouse_input_brush(event: InputEvent, world_position: Vector2, screen_position: Vector2, tile_position: Vector2):
	var brush_height: int = brush.size()
	var brush_width: int = brush[0].size()
	var brush_x: int = int(ceil(tile_position.x - brush_width * 0.5))
	var brush_y: int = int(ceil(tile_position.y - brush_height * 0.5))
	
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		# Place the brush
		var command := AddBrushCommand.new()
		command.brush = brush.duplicate()
		command.brush_x = brush_x
		command.brush_y = brush_y
		command.execute(tile_map, tile_mapper)
		add_undo_command(command)
	
	preview_brush(brush_x, brush_y)


func handle_mouse_input_select(event: InputEvent, world_position: Vector2, screen_position: Vector2, tile_position: Vector2):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		is_selecting = event.pressed
		if is_selecting:
			# Begin selection
			has_selection = false
			selection_box.rect_position = tile_map.map_to_world(tile_position)
			selection_box.rect_size = Vector2(_tile_width, _tile_height)
			selection_start_tile = tile_position
			selection_end_tile = tile_position
		else:
			# End selection
			has_selection = true
	if is_selecting:
		# Update the selection box
		selection_end_tile = tile_position
		var start_tile_world = tile_map.map_to_world(selection_start_tile)
		var end_tile_world = tile_map.map_to_world(selection_end_tile)
		selection_box.rect_position = Vector2(
			min(start_tile_world.x, end_tile_world.x),
			min(start_tile_world.y, end_tile_world.y)
		)
		selection_box.rect_size = Vector2(
			max(start_tile_world.x, end_tile_world.x) + _tile_width,
			max(start_tile_world.y, end_tile_world.y) + _tile_height
		) - selection_box.rect_position


func handle_mouse_input_metadata(event: InputEvent, world_position: Vector2, screen_position: Vector2, tile_position: Vector2):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		var tile = tile_map.get_cellv(tile_position)
		# Show the dialog
		if tile >= 0:
			emit_signal("tile_properties_selected", tile_position, tile)


func zoom_out():
	camera_zoom /= 0.75
	$Camera2D.zoom = Vector2.ONE * camera_zoom


func zoom_in():
	camera_zoom *= 0.75
	$Camera2D.zoom = Vector2.ONE * camera_zoom


func reset_zoom():
	camera_zoom = 1.0
	$Camera2D.zoom = Vector2.ONE


func select_palette_item(index: int):
	palette_tile_index = index
	_tile_index = index
	
	tile_rotation_index = 0
	preview.rotation = 0
	
	var tile_set = get_tile_set()
	preview.texture = tile_set.tile_get_texture(index)
	preview.region_enabled = true
	preview.region_rect = tile_set.tile_get_region(index)
	
	if current_tool != Tool.PLACE and current_tool != Tool.RECTANGLE_PLACE:
		set_tool(Tool.PLACE, true)


func set_tool(new_tool, should_emit: bool = false):
	match new_tool:
		Tool.PLACE:
			_tile_index = palette_tile_index
			preview.show()
			hover_cursor.hide()
			clear_tile_map_preview()
			hide_metadata_overlay()
			deselect()
		Tool.RECTANGLE_PLACE:
			_tile_index = palette_tile_index
			preview.show()
			hover_cursor.hide()
			clear_tile_map_preview()
			hide_metadata_overlay()
			deselect()
		Tool.BRUSH:
			if brush == []:
				# Only allow changing to brush when brush isn't empty
				return
			preview.hide()
			hover_cursor.hide()
			selection_box.hide()
			clear_tile_map_preview()
			hide_metadata_overlay()
			deselect()
		Tool.ERASE:
			_tile_index = -1
			preview.hide()
			hover_cursor.show()
			selection_box.hide()
			clear_tile_map_preview()
			hide_metadata_overlay()
			deselect()
		Tool.SELECT:
			preview.hide()
			hover_cursor.hide()
			selection_box.show()
			hide_metadata_overlay()
			clear_tile_map_preview()
		Tool.METADATA:
			tile_mapper.verify_all_tile_properties()
			preview.hide()
			hover_cursor.show()
			clear_tile_map_preview()
			show_metadata_overlay()
			deselect()
	
	current_tool = new_tool
	
	if should_emit:
		emit_signal("tool_changed", new_tool)


func deselect():
	has_selection = false
	selection_box.rect_size = Vector2.ZERO


func delete_selected_tiles():
	if has_selection:
		var to_delete = get_selected_tiles()
		var delete_command := AddTileCommand.new(-1, false, false, false)
		for tile in to_delete:
			delete_command.add_tile_position(self, tile_map, tile.x, tile.y)
		delete_command.execute(tile_map)
		add_undo_command(delete_command)
		
		deselect()


func preview_brush(brush_x: int, brush_y: int):
	var tiles: Array
	for y in brush.size():
		for x in brush[y].size():
			if brush[y][x] == null:
				continue
			brush[y][x].x = x + brush_x
			brush[y][x].y = y + brush_y
			tiles.append(brush[y][x])
	preview_tiles(tiles)


# This function expects an array of TileData
func preview_tiles(tiles: Array):
	clear_tile_map_preview()
	for tile in tiles:
		overlay_tile_map.set_cell(tile.x, tile.y, tile.tile_type, tile.flip_x, tile.flip_y, tile.transposed)


func clear_tile_map_preview():
	overlay_tile_map.clear()


func show_metadata_overlay():
	var all_tiles = get_all_tiles()
	for tile in all_tiles:
		var meta_properties = tile_mapper.get_meta_properties(tile.tile_type)
		if meta_properties.size() > 0:
			var is_edited = false
			var instance_properties = tile_mapper.get_instance_properties(Vector2(tile.x, tile.y))
			for key in instance_properties:
				is_edited = is_edited or instance_properties[key].is_edited()
			var icon
			if is_edited:
				icon = preload("res://icons/meta_data_edited.tscn").instance()
			else:
				icon = preload("res://icons/meta_data_unedited.tscn").instance()
			meta_data_overlay.add_child(icon)
			icon.position = (Vector2(tile.x, tile.y) + 0.5 * Vector2.ONE) * Vector2(_tile_width, _tile_height)
	meta_data_overlay.show()


func hide_metadata_overlay():
	while meta_data_overlay.get_child_count() > 0:
		var child = meta_data_overlay.get_child(0)
		meta_data_overlay.remove_child(child)
		child.queue_free()
	meta_data_overlay.hide()


func refresh_metadata_overlay():
	if meta_data_overlay.visible:
		hide_metadata_overlay()
		show_metadata_overlay()


func _on_InstanceDataEditorWindow_instance_properties_changed():
	refresh_metadata_overlay()


func _on_MetaDataEditorWindow_closed():
	refresh_metadata_overlay()


func set_tile_set(tile_set: TileSet, tile_width: int, tile_height: int):
	tile_map.tile_set = tile_set
	tile_map.cell_size = Vector2(tile_width, tile_height)
	overlay_tile_map.tile_set = tile_set
	overlay_tile_map.cell_size = Vector2(tile_width, tile_height)
	_tile_width = tile_width
	_tile_height = tile_height
	$HoverCursor.rect_size = Vector2(tile_width, tile_height)


func get_tile_set() -> TileSet:
	return tile_map.tile_set


func get_tile_rotation(x: int, y: int) -> int:
	var x_flipped: bool = tile_map.is_cell_x_flipped(x, y) # true when 1 or 2 (90 or 180)
	var y_flipped: bool = tile_map.is_cell_y_flipped(x, y) # true when 2 or 3 (180 or 270)
	var transposed: bool = tile_map.is_cell_transposed(x, y) # true when 1 or 3 (90 or 270)
	
	return Utility.get_tile_rotation_from_bools(x_flipped, y_flipped, transposed)


func get_selected_tiles() -> Array:
	var selected_tiles = []
	var top_left := Vector2(
		min(selection_start_tile.x, selection_end_tile.x),
		min(selection_start_tile.y, selection_end_tile.y)
		)
	var bottom_right := Vector2(
		max(selection_start_tile.x, selection_end_tile.x),
		max(selection_start_tile.y, selection_end_tile.y)
	)
	for y in range(top_left.y, bottom_right.y + 1):
		for x in range(top_left.x, bottom_right.x + 1):
			var tile_id = tile_map.get_cell(x, y)
			if tile_id >= 0:
				var tile_data := TileData.new(x, y, tile_id)
				tile_data.flip_x = tile_map.is_cell_x_flipped(x, y)
				tile_data.flip_y = tile_map.is_cell_y_flipped(x, y)
				tile_data.transposed = tile_map.is_cell_transposed(x, y)
				if tile_mapper.instance_properties.has(Vector2(x, y)):
					tile_data.instance_properties = tile_mapper.instance_properties[Vector2(x, y)]
				selected_tiles.append(tile_data)
	return selected_tiles


func create_brush_from_selected():
	var min_x: int = min(selection_start_tile.x, selection_end_tile.x)
	var max_x: int = max(selection_start_tile.x, selection_end_tile.x)
	var min_y: int = min(selection_start_tile.y, selection_end_tile.y)
	var max_y: int = max(selection_start_tile.y, selection_end_tile.y)
	
	brush = []
	for y in range(min_y, max_y + 1):
		brush.append([])
		brush[-1].resize(max_x - min_x + 1)
	
	for tile in get_selected_tiles():
		brush[tile.y - min_y][tile.x - min_x] = tile
	
	emit_signal("brush_changed", brush)


func get_all_tiles() -> Array:
	var all_tile_positions: Array = tile_map.get_used_cells()
	var all_tiles := []
	
	for pos in all_tile_positions:
		all_tiles.append(TileData.new(
			pos.x,
			pos.y,
			tile_map.get_cellv(pos),
			tile_map.is_cell_x_flipped(pos.x, pos.y),
			tile_map.is_cell_y_flipped(pos.x, pos.y),
			tile_map.is_cell_transposed(pos.x, pos.y),
			tile_mapper.get_instance_properties(pos)
		))
	
	return all_tiles


# Represents all data of a single tile
class TileData:
	var tile_type: int
	var x: int
	var y: int
	var flip_x: bool
	var flip_y: bool
	var transposed: bool
	var instance_properties: Dictionary = {}
	var rotation: int # In degrees, clockwise
	
	func _init(_x, _y, _tile_type, _flip_x: bool = false, _flip_y: bool = false, _transposed: bool = false, _instance_properties: Dictionary = {}):
		x = _x
		y = _y
		tile_type = _tile_type
		flip_x = _flip_x
		flip_y = _flip_y
		transposed = _transposed
		instance_properties = _instance_properties
		rotation = Utility.get_tile_rotation_from_bools(flip_x, flip_y, transposed)


"""######################
Saving & Loading
######################"""


func load_tile_data(all_tiles):
	tile_map.clear()
	
	for tile in all_tiles:
		var x_flipped = tile.rotation == 90 or tile.rotation == 180
		var y_flipped = tile.rotation == 180 or tile.rotation == 270
		var transposed = tile.rotation == 90 or tile.rotation == 270
		
		tile_map.set_cell(tile.position.x, tile.position.y, tile.type, x_flipped, y_flipped, transposed)


"""######################
Undo & redo
######################"""


func add_undo_command(command):
	undo_history.append(command)
	redo_history.clear()
	
	tile_mapper.has_unsaved_changes = true


func undo():
	if undo_history.size() > 0:
		var command: Command = undo_history.pop_back()
		command.undo(tile_map, tile_mapper)
		redo_history.append(command)
		tile_mapper.has_unsaved_changes = true


func redo():
	if redo_history.size() > 0:
		var command: Command = redo_history.pop_back()
		command.execute(tile_map, tile_mapper)
		undo_history.append(command)
		tile_mapper.has_unsaved_changes = true

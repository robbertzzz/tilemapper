class_name AddMetaPropertyCommand
extends Command


var property_type: int # MetaProperty.PropertyType
var property_name: String
var default_value
var tile_type: int


func execute(tile_map: TileMap = null, tile_mapper = null):
	var meta_property := MetaProperty.new()
	meta_property.property_type = property_type
	meta_property.property_name = property_name
	meta_property.set_default_value(default_value)
	tile_mapper.add_meta_property(tile_type, meta_property)


func undo(tile_map: TileMap = null, tile_mapper = null):
	var meta_property = tile_mapper.get_meta_property_by_name(tile_type, property_name)
	assert(meta_property != null)
	tile_mapper.remove_meta_property(tile_type, meta_property)

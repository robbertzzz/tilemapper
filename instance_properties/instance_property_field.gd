class_name InstancePropertyField
extends HBoxContainer


signal value_change(instance_property, new_value)


var instance_property: InstanceProperty


func init(_instance_property: InstanceProperty):
	instance_property = _instance_property
	
	match instance_property.meta_property.property_type:
		MetaProperty.PropertyType.BOOL:
			$Value.pressed = instance_property.bool_value
		MetaProperty.PropertyType.INT:
			$Value.value = instance_property.int_value
		MetaProperty.PropertyType.FLOAT:
			$Value.value = instance_property.float_value
		MetaProperty.PropertyType.STRING:
			$Value.text = instance_property.string_value
	
	$Label.text = instance_property.meta_property.property_name


func _on_Value_toggled(button_pressed):
	emit_signal("value_change", instance_property, button_pressed)


func _on_Value_value_changed(value):
	emit_signal("value_change", instance_property, value)


# For the String version, listening to text change would be horrible for undo/redo
func _on_Value_focus_exited():
	var text = $Value.text
	if text != instance_property.string_value:
		emit_signal("value_change", instance_property, $Value.text)
